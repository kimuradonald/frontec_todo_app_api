class Todos < Grape::API
  namespace :users do
    route_param :user_id do
      namespace :todos do

        # swagger documentation.
        # headers({ :'uuid' => { description: 'custom header', required: false } })

        rescue_from ActiveRecord::RecordNotFound do
          record_not_found_error!
        end

        before do
          authenticate_user!
        end

        helpers do

          # 認証エラー時のエラー応答を生成する処理です。
          def authentication_error!
            error!({ status: 401, message: 'Unauthorized' }, 401)
          end

          # Record Not Found時のエラー応答を生成する処理です。
          def record_not_found_error!
            error!({ status: 404, message: 'Record Not Found' }, 404)
          end

          # リクエストヘッダーをもとに認証する処理です。
          # 実行後は@userから認証されたUserを参照可能です。
          def authenticate_user!
            @user = User.find_by_id(params[:user_id])
            # authentication_error! unless @user && @user.valid_token?(request.headers['Access-Token'], request.headers['Client'])
          end
        end

        
        desc 'return all todos'
        get do
          @todos = @user.todos.all
        end

        desc 'return a todo'
          params do
            requires :id, type: Integer
          end
          # failure [[401, 'Unauthorized']]
        get ':id' do
          @todo = @user.todos.find(params[:id])
        end

        desc 'create a todo'
        params do
          requires :title, type: String
          requires :memo, type: String
          requires :deadline, type: Date
        end
        post do
          @todo = Todo.create(
            user_id: params[:user_id],
            title: params[:title],
            memo: params[:memo],
            deadline: params[:deadline]
          )
        end

        desc 'edit a todo'
        params do
          requires :id, type: Integer
          optional :title, type: String
          optional :memo, type: String
          optional :deadline, type: Date
        end
        put ':id' do
          @todo = @user.todos.find(params[:id])
          @todo.update_attributes(
            {
              title: params[:title],
              memo: params[:memo],
              deadline: params[:deadline]
            }.reject{ |k,v| !v.present? }
          )
          # 応答JSONに更新後のモデルを出力するために記述
          @todo
        end

        desc 'delete a todo'
        params do
          requires :id, type: Integer
        end
        delete ':id' do
          @todo = @user.todos.find(params[:id])
          @todo.destroy
        end

        resource 'complete' do
          desc 'make a todo completed'
          params do
            requires :id, type: Integer
          end
          put ':id' do
            @todo = @user.todos.find(params[:id])
            @todo.update(
              completed: true
            )
            # 応答JSONに更新後のモデルを出力するために記述
            @todo
          end
        end
      end
    end
  end
end