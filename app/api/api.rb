class API < Grape::API
  prefix 'api'
  format :json
  mount Todos

  add_swagger_documentation(
    schemes: ['http', 'https']
  )
end