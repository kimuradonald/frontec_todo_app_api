class AddUserIdColumnToTodos < ActiveRecord::Migration[5.1]
  def change
    add_column :todos, :user_id, :integer, default: 0, null: false
  end
end
